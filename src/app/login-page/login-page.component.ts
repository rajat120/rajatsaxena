import { Component, OnInit } from '@angular/core';
import { AuthService } from "angularx-social-login";
import { SocialUser } from "angularx-social-login";
import { GoogleLoginProvider} from "angularx-social-login";
import { Router, ActivatedRoute } from '@angular/router';
@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.css']
})
export class LoginPageComponent implements OnInit {


  private user: SocialUser;
  private loggedIn: boolean;


  constructor(private authService : AuthService, private router : Router) { }

  ngOnInit() {
    this.authService.authState.subscribe((user) => {
      this.user = user;
      this.loggedIn = (user != null);
      console.log(user);

      if(user!==null){
        this.router.navigate(['homepage']);
        localStorage.setItem('useremail',user.email);
      }
      else{
        this.router.navigate(['login']);
      }
    });
  }

  private SignIn(){
      this.authService.signIn(GoogleLoginProvider.PROVIDER_ID);
    }
  
  }


