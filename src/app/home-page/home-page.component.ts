import { Component, OnInit } from '@angular/core';
import { AuthService } from 'angularx-social-login';
import { SocialUser } from 'angularx-social-login';
import { Router, ActivatedRoute } from '@angular/router';
import { ToDoDataServiceService} from '../to-do-data-service.service';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.css']
})
export class HomePageComponent implements OnInit {
  private userDisplayInformation;
  constructor(private authService: AuthService, private router: Router, private todoservice : ToDoDataServiceService) {

   }
   private user : SocialUser;

  ngOnInit() {
   this.userDisplayInformation = [{label:'Name',property: 'name'}, {label:'Email',property: 'email'}, {label:'Photo',property: 'photoUrl'}];
    
    this.authService.authState.subscribe((user) => {
      this.user = user;


     // console.log(this.user);
    });
  }
  

  private SignOut(){
   //console.log(this.user);
    console.log(this.user);
      this.authService.signOut();
      
      this.router.navigate(['login']);
  }

  private ToDoList(){

    if(localStorage.getItem('useremail')!=this.user.email){
      const userObj = {
        "email" : this.user.email,
        "todos" : []
      }
      this.todoservice.postUser(userObj).subscribe(res => {
        console.log(res);
      });
    }
    

    
    this.router.navigate(['todo']);
  }

}
