
import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { HomePageComponent } from './home-page/home-page.component';
import { LoginPageComponent} from './login-page/login-page.component';
import { ToDoComponent} from './to-do/to-do.component';


const routes: Routes = [
  {
    path: '',
    component: LoginPageComponent
  },
  {
    path: 'login',
    component: LoginPageComponent
   
  },
  {
      path: 'homepage',
      component: HomePageComponent
  },
  {
    path: 'todo',
    component: ToDoComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class AppRoutingModule {
  static components = [
    LoginPageComponent,
    HomePageComponent,
    ToDoComponent

  ];
}
