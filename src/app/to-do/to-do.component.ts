import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
// import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
// import {DialogDataExampleDialogComponent} from '../dialog-data-example-dialog/dialog-data-example-dialog.component';
import { ToDoDataServiceService } from '../to-do-data-service.service';
@Component({
  selector: 'app-to-do',
  templateUrl: './to-do.component.html',
  styleUrls: ['./to-do.component.css']
})
export class ToDoComponent implements OnInit {
  @ViewChild('titleModel') titleModel: ElementRef;

  constructor(private router: Router, private tododataservice: ToDoDataServiceService) { }
  private d;
  private display = "none";
  private displayEdit = "none";
  private opacity = 1;
  private inputText: String;
  private todoList = [
    // {description:' I have to go to shopping mall.'}
  ];
  private editData: Object;
  private editIndex: any;
  ngOnInit() {
    this.d = new Date();
    let useremail = localStorage.getItem('useremail');
    this.tododataservice.getTodos(useremail)
      .subscribe(res => {
        console.log(res);
        this.todoList = res;

      });

  }

  private HomeProfile() {
    this.router.navigate(['homepage']);
  }

  private openModel() {
    this.display = 'block';
    this.opacity = 0.6;
  }
  openDialog() {

  }

  closeFunction() {
    this.display = 'none';
    this.displayEdit = 'none';
    this.opacity = 1;
  }

  private saveChanges(data) {
    this.inputText = '';
    this.closeFunction();
    const obj = { description: data  , useremailid: localStorage.getItem('useremail')};
    // obj.sno = this.todoList.length + 1;
   
   
    this.todoList.push(obj);

    this.tododataservice.postToDo(obj).subscribe(res => {
      console.log(res);
    })
  }
  
  // delete functionality..
  private deleteToDo(index,description) {

   this.tododataservice.getTodos(localStorage.getItem('useremail')).subscribe(res => {
      console.log(res);
      console.log(description);
   let objdeleteIndex;
      for(let n = 0; n < res.length;n++){
        if(res[n]['description'] === description){
           objdeleteIndex = res[n].id;
        }
      }
      console.log(objdeleteIndex);
     
      this.tododataservice.deleteTodo(objdeleteIndex).subscribe(res => {
        
        console.log(res);
        this.todoList.splice(index, 1);
      })
    });

  }

  // edit todo
  private editToDo(editData, index) {
    this.displayEdit = 'block';
    this.editData = editData;
    this.editIndex = index;

    console.log(index);

  }

  private saveEditChanges(data) {
    this.displayEdit = 'none';

    console.log(data);
    console.log(this.editIndex);
    this.todoList[this.editIndex].description = data;

    console.log(this.todoList);


  }
}

