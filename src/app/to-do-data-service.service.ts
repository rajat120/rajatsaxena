import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ToDoDataServiceService {

  constructor(private http: HttpClient) { }

  // get request based on email id
  public getTodos(useremail){
    return this.http.get<any>('http://localhost:3000/ToDo?useremailid='+ useremail);
  }

  // post a new user in the database
  public postUser(userobj){
    return this.http.post('http://localhost:3000/ToDo',userobj);
  }

  // edit a existing to do list for the user
  public editToDo(editObj){
    return this.http.put('http://localhost:3000/ToDo',editObj);
  }

  // delete a todo list item for the user
  public deleteTodo(index){
    return this.http.delete('http://localhost:3000/ToDo/'+index);
  }
  
  // post a new todo into the todo list.
  public postToDo(postObj){
    return this.http.post('http://localhost:3000/ToDo',postObj);
  }

}
