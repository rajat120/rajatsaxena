import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { HttpClientModule} from '@angular/common/http';
import { AppRoutingModule} from './app.routingmodule';
import { AppComponent } from './app.component';
import { LoginPageComponent } from './login-page/login-page.component';
import { HomePageComponent } from './home-page/home-page.component';
import { SocialLoginModule, AuthServiceConfig } from "angularx-social-login";
import { GoogleLoginProvider, FacebookLoginProvider, LinkedInLoginProvider} from "angularx-social-login";
import { SignUpComponent } from './sign-up/sign-up.component';
import { ToDoComponent } from './to-do/to-do.component';
import { FormsModule} from '@angular/forms';
import { NgxPaginationModule} from 'ngx-pagination';
 
let config = new AuthServiceConfig([
  {
    id: GoogleLoginProvider.PROVIDER_ID,
    provider: new GoogleLoginProvider("884243739751-6ahchicvtqpbesqkudpflf98ch37ch7d.apps.googleusercontent.com")
  }
]);

export function provideConfig() {
  return config;
}

@NgModule({
  declarations: [
    AppComponent,
    LoginPageComponent,
    HomePageComponent,
    SignUpComponent,
    ToDoComponent
    
  ],
  imports: [
    BrowserModule,
    SocialLoginModule,
    AppRoutingModule,
    FormsModule,
    NgxPaginationModule,
    HttpClientModule
  ],
  providers: [
    {
      provide: AuthServiceConfig,
      useFactory: provideConfig
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
