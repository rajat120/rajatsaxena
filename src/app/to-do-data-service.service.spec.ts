import { TestBed, inject } from '@angular/core/testing';

import { ToDoDataServiceService } from './to-do-data-service.service';

describe('ToDoDataServiceService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ToDoDataServiceService]
    });
  });

  it('should be created', inject([ToDoDataServiceService], (service: ToDoDataServiceService) => {
    expect(service).toBeTruthy();
  }));
});
